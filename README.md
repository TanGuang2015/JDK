### 融之家JS SDK DEMO(version 1.1.0)

## js

```js
  // 监听done事件，返回参数为e.data
    document.addEventListener("done", function(e) {
      document.getElementById('ur').innerHTML = e.detail;
    });
```

## html

```html
<a href="protocol://photo" class="weui_btn weui_btn_primary">调用相机</a>
<a href="protocol://image " class="weui_btn weui_btn_warn">调用图片</a>
```

#### 当前方法为同步执行，请在当前按钮上触发事件（click,tap等）并监听返回事件做回调处理,后续会提供更方便及兼容方法

    
    