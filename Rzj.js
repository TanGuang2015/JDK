; (function() {

  if (window.Rzj) {
    return;
  }

  var Rzj, dct;
  dct = window.document;
  Rzj = window.Rzj = {
    init: function() {
      // console.info('loaded');
    },
    callBack: function(callback) {
      if (typeof callback === 'function') {
        callback();
      }
    },
    fireEvent: function(type, data) {
      var event = document.createEvent('HTMLEvents');
      event.initEvent(type, true, true);
      event.data = data || {};
      event.eventName = type;
      event.target = dct;
      dct.dispatchEvent(event);
      return this;
    }
  };

  // 初始化
  Rzj.init();

})(window);


// 示例 由APP调用
// Rzj.fireEvent('done', 'Test://www.baidu.com');